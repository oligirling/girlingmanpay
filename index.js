const Stripe = require("stripe");

const corsHeaders = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET,HEAD,POST,OPTIONS",
    "Access-Control-Max-Age": "86400",
}

addEventListener("fetch", event => {
    event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {

    if (request.method === 'OPTIONS') {
        let respHeaders = {
            ...corsHeaders,
            // Allow all future content Request headers to go back to browser
            // such as Authorization (Bearer) or X-Client-Name-Version
            "Access-Control-Allow-Headers": request.headers.get("Access-Control-Request-Headers"),
        }
        return new Response(null, {
            headers: respHeaders,
        })
    }

    const key = request.headers.get('x-sausage')
    const url = new URL(request.url)
    const { searchParams } = url
    const type = url.pathname

    if (!key || !type) {
        return respond("Sausages")
    }

    if (type === '/payment') {
        const version = request.headers.get('x-version')
        if (!version) {
            return respond("No version (stage | prod)")
        }
        return await makePayment(searchParams, version)
    }

    if (type === '/create-order') {

        try {
            const formData = await request.formData()
            const orderData = {}
            for (const entry of formData.entries()) {
                orderData[entry[0]] = entry[1]
            }

            if (!orderData) {
                return respond("No order details")
            }
            await createOrder(searchParams, orderData)
            return respond("working", false)
        } catch (e) {
            return respond("Not working: " + e.message)
        }

    }

    if (type === '/list-orders') {
        const orders = await listOrders()
        return respond("working", false, {
            orders: orders
        })
    }


    if (type === '/list-accom') {
        const accom = await listAccom()
        return respond("working", false, {
            accom: accom
        })
    }



}

/**
 * Example response
 * {
  "From": "fun@girlingman.com",
  "To": "ol@h.com",
  "TemplateAlias": "receipt",
  "TemplateModel": {
    "product_url": "https://girlingman.com",
    "product_name": "GirlingMan",
    "name": "Oli G",
    "referral_link": "https://girlingman.com?ref=olig",
    "referred_by": "brian",
    "receipt_id": 1645036844623,
    "date": "2/16/2022, 6:40:44 PM",
    "receipt_details": [
      {
        "description": "GirlingMan ticket for Oli G",
        "forName": "Oli G",
        "amount": "75"
      },
      {
        "description": "Preorder of 10x beers for Oli G",
        "forName": "Oli G",
        "amount": "22.50"
      },
      {
        "description": "GirlingMan ticket for Beno",
        "forName": "Beno",
        "amount": "75"
      },
      {
        "description": "GirlingMan ticket for Tomo",
        "forName": "Tomo",
        "amount": "75"
      },
      {
        "description": "Preorder of 10x beers for Tomo",
        "forName": "Tomo",
        "amount": "22.50"
      },
      {
        "description": "6 meter (2x double beds with 2x single beds   wood fire stove)",
        "forName": [
          "Oli G",
          "Beno",
          "Tomo"
        ],
        "amount": "350"
      }
    ],
    "total": "620.00",
    "peopleInTent": [
      "Oli G",
      "Beno",
      "Tomo"
    ],
    "company_name": "GirlingMan LTD",
    "company_address": "2 Fleetwood Apartments, 2 Northwold Road, London, N16 7HG"
  }
}
 *
 *
 * @param orderData2
 * @param orderId
 */
async function getOrderData(orderData2, orderId)
{
    const usersEmail = orderData2.payee.email
    const usersName = orderData2.payee.name
    const weblink = 'https://girlingman.com'
    const refLink = `${weblink}?ref=${usersEmail}`
    const timestamp = Date.now()
    const humanReadableDateTime = new Date(timestamp).toLocaleString()
    const total = orderData2.total
    const referredBy = orderData2.referredBy

    const tickets = orderData2.tickets
    let ticketData = []
    let peopleInTent = []
    let hoomans = []
    let tentData = orderData2.tent
    for (const ticket of tickets) {
        ticketData.push({
            'description': 'GirlingMan ticket for '+ticket.name,
            'forName': ticket.name,
            'amount': ticket.price
        })
        hoomans.push({
            name: ticket.name,
            email: ticket.email,
            beers: ticket.beers,
            price: ticket.price,
            isDayTicket: ticket.isDayTicket ? ticket.isDayTicket : null
        })
        if (ticket.beers != 0) {
            ticketData.push({
                'description': 'Preorder of 10x beers for '+ticket.name,
                'forName': ticket.name,
                'amount': ticket.beers
            })
        }
        if (orderData2.hasOwnProperty('tent') && orderData2.tent.hasOwnProperty('code')) {
            peopleInTent.push(ticket.name)
        }
    }

    if (orderData2.hasOwnProperty('tent') && orderData2.tent.hasOwnProperty('code')) {
        ticketData.push({
            'description': orderData2.tent.name,
            'forName': peopleInTent,
            'amount': orderData2.tent.price
        })
    }

    return {
        "From": "fun@girlingman.com",
        "To": usersEmail,
        "TemplateAlias": "receipt",
        "TemplateModel": {
            "tentData": tentData,
            "listOfHumans": hoomans,
            "product_url": weblink,
            "product_name": "GirlingMan",
            "name": usersName,
            "referral_link": refLink,
            "referred_by": referredBy,
            "receipt_id": orderId,
            "date": humanReadableDateTime,
            "receipt_details": ticketData,
            "total": total,
            "peopleInTent": peopleInTent,
            "company_name": "GirlingMan LTD",
            "company_address": "2 Fleetwood Apartments, 2 Northwold Road, London, N16 7HG"
        }
    }

}

async function listOrders()
{
    const orders = await GIRLING_MAN_2022_ORDERS.list()
    const keys = orders.keys
    let data = {}
    for (const key of keys) {
        let orderId = key.name
        data[orderId] = await GIRLING_MAN_2022_ORDERS.get(orderId)
    }
    return data
}

async function listAccom()
{
    const accom = await GIRLING_MAN_ACCOM.list()
    const keys = accom.keys
    let data = {}
    for (const key of keys) {
        let code = key.name
        data[code] = await GIRLING_MAN_ACCOM.get(code)
    }
    return data
}

async function removeAccom(code)
{
    let qty = await GIRLING_MAN_ACCOM.get(code)
    qty--
    await GIRLING_MAN_ACCOM.put(code, qty)
}

async function createOrder(searchParams, order)
{
    const orderId = Date.now()
    const string = JSON.stringify(order)
    const obj = JSON.parse(string)
    const orderData = JSON.parse(obj.data)
    const orderData2 = JSON.parse(orderData)

    if (orderData2.hasOwnProperty('tent') && orderData2.tent.hasOwnProperty('code')) {
        await removeAccom(orderData2.tent.code)
    }

    const orderDataFull = await getOrderData(orderData2, orderId)
    await GIRLING_MAN_2022_ORDERS.put(orderId, JSON.stringify(orderDataFull))

    await sendInvoice(orderDataFull)
}

async function sendInvoice(orderDataFull)
{
    try {
        const call = await fetch('https://api.postmarkapp.com/email/withTemplate', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-Postmark-Server-Token': 'd41ae6e4-821c-41a2-bce0-54d76e0fba9f',
            },
            body: JSON.stringify(orderDataFull)
        })
        const json = await call.json()
        console.log(json)
    } catch (e) {
        console.error(e.message)
    }

}

async function makePayment(searchParams, version)
{
    let amount = searchParams.get('amount')
    if (!amount) {
        return respond("Missing Amount")
    }
    let apiKey = STRIPE_PRIVATE_KEY_SANDBOX;
    if (version === 'prod') {
        apiKey = STRIPE_PRIVATE_KEY;
    }

    try {

        const stripe = Stripe(apiKey, {
            // Cloudflare Workers use the Fetch API for their API requests.
            httpClient: Stripe.createFetchHttpClient()
        });

        const paymentIntent = await stripe.paymentIntents.create({
            amount: amount*100,
            currency: "gbp",
            automatic_payment_methods: {
                enabled: true,
            },
        });
        if (!paymentIntent.client_secret) {
            throw new Error('No client secret id. Response was ' + JSON.stringify(paymentIntent))
        }
        return respond("working", false, {public_id: paymentIntent.client_secret})
    } catch (e) {
        console.log('Checkout error: ' + e.message)
        return respond(e.message)
    }
}

function respond(message, error = true, data = {})
{
    return new Response(JSON.stringify({
        ...data,
        'error': error,
        'message': message,
    }), {
        headers: corsHeaders,
    })
}
